import { getAllUnits, updateUnit } from "../services/Unit";

export default {
  namespace: "unit",

  state: {
    unit_list: [],
  },

  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      // eslint-disable-line
    },
  },

  effects: {
    // 获取当前项目下的所有图表
    *getAllUnits({ payload }, { call, put }) {
      // eslint-disable-line
      const result = yield call(getAllUnits, payload);
      yield put({ 
        type: "updateState",
        payload: {
          unit_list: result,
        },
      });
      return result;
    },

    // 更新项目下的某个图表
    *updateUnit({ payload }, { call, put }) {
      const result = yield call(updateUnit, payload);
      return result;
    }
  },
};

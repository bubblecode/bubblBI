import { getConfig, updateConfig } from "../services/setting";

export default {
  namespace: "setting",

  state: {
    ToolbarAutoHideDelay: 2000,
  },

  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      // eslint-disable-line
    },
  },

  effects: {
    // 获取全局配置信息
    *getGlobalConfig({ payload }, { call, put }) {
      // eslint-disable-line
      const result = yield call(getConfig, payload);
      yield put({
        type: "updateState",
        payload: {
          ...result,
        },
      });
      return result;
    },

    // 更新全局配置信息
    *updateGlobalConfig({ payload }, { call, put }) {
      const result = yield call(updateConfig, payload);
      // 如果修改成功，调用更新，失败抛出异常
      if (result.message === "OK") {
        yield call(getConfig, {
          user_id: payload.user_id,
        });
      }
      return result;
    },
  },
};

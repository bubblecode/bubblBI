import { getUserInfo } from "../services/basic";

export default {

  namespace: 'basic',

  state: {
    user_info: {},
  },

  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {  // eslint-disable-line
    },
  },

  effects: {
    *getUserData({ payload }, { call, put }) {  // eslint-disable-line
      const result = yield call(getUserInfo, payload);
      yield put({ 
        type: "updateState",
        payload: {
          user_info: result,
        },
      });
      return result;
    },
  },

};

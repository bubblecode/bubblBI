import { getProjectTree, getProjectInfo } from "../services/project";

export default {
  namespace: "project",

  state: {
    project_tree: [],
    cur_project: {},
  },

  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      // eslint-disable-line
    },
  },

  effects: {
    // 获取当前用户的项目目录树
    *getProjectTree({ payload }, { call, put }) {
      // eslint-disable-line
      const result = yield call(getProjectTree, payload);
      yield put({ 
        type: "updateState",
        payload: {
          project_tree: result,
        },
      });
      return result;
    },

    // 获取项目信息
    *getProjectInfo({ payload }, { call, put }) {
      const result = yield call(getProjectInfo, payload);
      yield put({ 
        type: "updateState",
        payload: {
          cur_project: result,
        },
      });
      return result;
    }
  },
};

import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import PageFooter from "../PageFooter";
import GlobalSetting from "../GlobalSetting";
import ProjectPanel from "../ProjectPanel";
import DesignPanel from "../DesignPanel";
import PluginPanel from "../PluginPanel";
import { BAR_TYPE } from "../../api/api";

export default class Contain extends Component {
  static propTypes = {
    curType: PropTypes.string,
    curProject: PropTypes.object,
  };

  renderContain = () => {
    switch (this.props.curType) {
      case BAR_TYPE.SETTING:
        return <GlobalSetting />;
      case BAR_TYPE.PROJECT:
        return <ProjectPanel curProject={this.props.curProject} />;
      case BAR_TYPE.DESIGN:
        return <DesignPanel curProject={this.props.curProject} />;
      case BAR_TYPE.PLUGIN:
        return <PluginPanel curProject={this.props.curProject} />;
      default:
        break;
    }
    return false;
  };

  get isPanelState() {
    return (
      this.props.curType === BAR_TYPE.PROJECT ||
      this.props.curType === BAR_TYPE.DESIGN
    );
  }

  render() {
    return (
      <Fragment>
        <div style={{ width: "100%" }}>
          <div
            style={{
              position: "relative",
              left: "-5px",
              height: "94vh",
              padding: "10px",
              borderRadius: "8px",
              backgroundColor: this.isPanelState ? "#EEE" : "#EFF",
              boxShadow: this.isPanelState ? '0px 0px 10px #000' : 'none',
            }}
          >
            {this.renderContain()}
          </div>
          <PageFooter />
        </div>
      </Fragment>
    );
  }
}

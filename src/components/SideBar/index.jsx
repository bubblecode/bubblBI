import React, { Component } from "react";
import { connect } from "dva";
import PropTypes from "prop-types";
import { List, Tooltip, Drawer, Alert, Button } from "antd";
import {
  InboxOutlined,
  SettingOutlined,
  ShoppingOutlined,
  MessageOutlined,
  UngroupOutlined,
} from "@ant-design/icons";
import DesignTree from "./components/DesignTree/index";
import PluginList from "./components/PluginList/index";
import ProjectTree from "./components/ProjectTree/index";
import SettingAnchor from "./components/SettingAnchor/index";
import { BAR_TYPE } from "../../api/api";
import styles from "./index.scss";

const ListItem = List.Item;
const designData = [
  {
    title: "类别1",
    key: "0-0",
    children: [
      {
        title: "自定义页面1",
        key: "0-0-0",
        isLeaf: true,
      },
    ],
  },
  {
    title: "类别2",
    key: "0-1",
    children: [
      {
        title: "自定义页面1",
        key: "0-1-0",
        isLeaf: true,
      },
      {
        title: "自定义页面2",
        key: "0-1-1",
        isLeaf: true,
      },
    ],
  },
];
const listData = [
  {
    title: <div className={styles.sidebarItem}><InboxOutlined /></div>,
    tips: "我的项目",
    key: "project",
  },
  {
    title: <div className={styles.sidebarItem}><ShoppingOutlined /></div>,
    tips: "插件市场",
    key: "plugin",
  },
  {
    title: <div className={styles.sidebarItem}><UngroupOutlined /></div>,
    tips: "设计台",
    key: "design",
  },
  {
    title: <div className={styles.sidebarItem}><MessageOutlined /></div>,
    tips: "消息中心",
    key: "message",
  },
  {
    title: <div className={styles.sidebarItem}><SettingOutlined /></div>,
    tips: "设置",
    key: "setting",
  },
];
const msgData = [
  {
    message_id: 1,
    type: "info",
    title: "提示",
    description: "在这里进行提示",
  },
  {
    message_id: 2,
    type: "success",
    title: "申请成功",
    description: "成功加入项目组 23299",
  },
  {
    message_id: 3,
    type: "warning",
    title: "警告",
    description: "资源不足，请及时清理在线空间",
  },
  {
    message_id: 4,
    type: "error",
    title: "错误",
    description: "项目A，运行错误",
  },
  {
    message_id: 5,
    type: "info",
    title: "提示",
    description: "在这里进行提示",
  },
];

class SideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      msgCenterVisible: false,
    };
  }

  onItemClick = (type) => {
    if (type === BAR_TYPE.MESSAGE) {
      this.onMessageCenterOpen();
    } else if (this.props.curType !== type) {
      this.props.onContainTypeChange(type);
    }
  };

  onMessageCenterOpen = () => {
    this.setState({
      msgCenterVisible: true,
    });
  };

  onMessageCenterClose = () => {
    this.setState({
      msgCenterVisible: false,
    });
  };


  renderExtraMenu = () => {
    switch (this.props.curType) {
      case BAR_TYPE.PROJECT:
        return <ProjectTree onProjectClick={this.props.onProjectClick}/>;
      case BAR_TYPE.PLUGIN:
        return <PluginList />;
      case BAR_TYPE.DESIGN:
        return <DesignTree treeData={designData} />;
      case BAR_TYPE.SETTING:
        return <SettingAnchor />;
      default:
        break;
    }
    return false;
  };

  renderMessageCenter = (messageList) => {
    return (
      <Drawer
        title="消息中心"
        width={350}
        placement="right"
        closable={false}
        onClose={this.onMessageCenterClose}
        visible={this.state.msgCenterVisible}
        footer={
          <div>
            <Button type="link">全部清除</Button>
          </div>
        }
      >
        {messageList.map((msg) => (
          <div style={{ margin: "10px 0" }} key={msg.message_id}>
            <Alert
              message={msg.title}
              description={msg.description}
              type={msg.type}
              showIcon
              closable
            />
          </div>
        ))}
      </Drawer>
    );
  };

  render() {
    return (
      <div>
        <div className={styles.content}>
          <aside>
            <List
              size="large"
              style={{
                fontSize: "20px",
                borderRight: "1px solid #AAA",
                height: "100%",
              }}
              dataSource={listData}
              renderItem={(item) => (
                <Tooltip title={item.tips} placement="right">
                  <ListItem
                    key={item.key}
                    onClick={this.onItemClick.bind(this, item.key)}
                  >
                    {item.title}
                  </ListItem>
                </Tooltip>
              )}
            />
          </aside>
          <div style={{ width: "100%", padding: "2px" }}>
            {this.renderExtraMenu()}
          </div>
        </div>
        {this.renderMessageCenter(msgData)}
      </div>
    );
  }
}

SideBar.propTypes = {
  containType: PropTypes.string,
  onContainTypeChange: PropTypes.func,
  onProjectClick: PropTypes.func,
};

export default connect(({ project }) => ({ project }))(SideBar);
import React, { Component } from "react";
import { connect } from "dva";
import { Tree, Divider, Tooltip } from "antd";
import {
  FolderAddOutlined,
  FileAddOutlined,
  SyncOutlined,
} from "@ant-design/icons";

const { DirectoryTree, TreeNode } = Tree;

class ProjectTree extends Component {
  componentDidMount() {
    this.props.dispatch({
      type: "project/getProjectTree",
      payload: {
        user_id: 10001,
      },
    });
  }

  onSelect = (keys, info) => {
    const { node } = info;
    if (node.isLeaf) {
      this.props.onProjectClick(keys);
    }
  };

  onExpand = () => {
    console.log("Trigger Expand");
  };

  renderTreeNodes = (treeData) => {
    return treeData.map((node) => {
      if (node.children && node.children.length > 0) {
        return (
          <TreeNode
            key={node.project_id}
            title={node.title}
            isLeaf={node.project_type === 1}
          >
            {this.renderTreeNodes(node.children)}
          </TreeNode>
        );
      }
      return (
        <TreeNode
          key={node.project_id}
          title={node.title}
          isLeaf={node.project_type === 1}
        />
      );
    });
  };

  render() {
    const { project_tree: treeData } = this.props.project;

    return (
      <div>
        <header
          style={{
            display: "grid",
            padding: "5px",
            gridTemplateColumns: "1fr 1fr 1fr",
            textAlign: "center",
          }}
        >
          <Tooltip title="新建文件夹">
            <FolderAddOutlined />
          </Tooltip>
          <Tooltip title="新建面板">
            <FileAddOutlined />
          </Tooltip>
          <Tooltip title="刷新">
            <SyncOutlined />
          </Tooltip>
        </header>
        <Divider plain style={{ margin: "6px 0" }}></Divider>
        <DirectoryTree
          defaultExpandAll
          onSelect={this.onSelect}
          onExpand={this.onExpand}
        >
          {treeData && this.renderTreeNodes(treeData)}
        </DirectoryTree>
      </div>
    );
  }
}

export default connect(({ project }) => ({ project }))(ProjectTree);

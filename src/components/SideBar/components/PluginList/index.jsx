import React, { Component } from "react";
import { Select, List, message, Tooltip } from "antd";
import { AppstoreOutlined } from "@ant-design/icons";
import styles from "./index.scss";

const { Option } = Select;

const pluginInfo = [
  {
    plugin_id: 1,
    title: "A Good Template Plugin",
    description: "Here is the description of the plugin.",
  },
  {
    plugin_id: 2,
    title: "Theme Plugin",
    description: "Here is the description of the plugin",
  },
  {
    plugin_id: 3,
    title: "Icon Theme Plugin",
    description: "Here is the description of the plugin",
  },
  {
    plugin_id: 4,
    title: "Template Plugin 2",
    description: "Here is the description of the plugin",
  },
];
export default class PluginList extends Component {
  static propTypes = {
  };

  getPluginDetail = (pluginId) => {
    message.info(`查看插件 ${pluginId} 信息`);
  };

  render() {
    return (
      <div style={{ padding: "5px" }}>
        <Select
          style={{ width: "100%" }}
          allowClear
          showArrow={false}
          showSearch
          placeholder="Search Plugin"
        >
          {pluginInfo.map((plg) => (
            <Option key={plg.plugin_id}>{plg.title}</Option>
          ))}
        </Select>
        <List
          size="small"
          dataSource={pluginInfo}
          renderItem={(item) => (
            <List.Item key={item.plugin_id}>
              <List.Item.Meta
                avatar={<AppstoreOutlined />}
                title={
                  <Tooltip placement="right" title={item.title}>
                    <p
                      className={styles.pluginTitle}
                      onClick={this.getPluginDetail.bind(null, item.plugin_id)}
                    >
                      {item.title}
                    </p>
                  </Tooltip>
                }
                description={
                  <span style={{ fontSize: "12px" }}>{item.description}</span>
                }
              />
            </List.Item>
          )}
        ></List>
      </div>
    );
  }
}

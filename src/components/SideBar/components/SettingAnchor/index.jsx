import { Anchor } from "antd";
import React from "react";

const { Link } = Anchor;
export default function SettingAnchor() {

  const handleClick = (e, link) => {
    e.preventDefault();
  };

    return (
      <div>
        <Anchor onClick={handleClick}>
          <Link href="#header" title="全局配置">
            <Link href="#basic-config" title="基本设置" />
            <Link href="#project-config" title="项目配置" />
            <Link href="#plugin-config" title="插件管理" />
            <Link href="#advanced-config" title="高级设置" />
          </Link>
        </Anchor>
      </div>
    );
}

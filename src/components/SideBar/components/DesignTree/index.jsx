import React, { Component } from "react";
import PropTypes from "prop-types";
import { Tree, Divider, Tooltip } from "antd";
import {
  FolderAddOutlined,
  FileAddOutlined,
  SyncOutlined,
} from "@ant-design/icons";

const { DirectoryTree } = Tree;

export default class DesignTree extends Component {
  static propTypes = {
    treeData: PropTypes.array,
  };

  onSelect = (keys, info) => {
    console.log("Trigger Select", keys, info);
  };

  onExpand = () => {
    console.log("Trigger Expand");
  };

  render() {
    return (
      <div>
        <header
          style={{
            display: "grid",
            padding: "5px",
            gridTemplateColumns: "1fr 1fr 1fr",
            textAlign: "center",
          }}
        >
          <Tooltip title="新建文件夹">
            <FolderAddOutlined />
          </Tooltip>
          <Tooltip title="新建面板">
            <FileAddOutlined />
          </Tooltip>
          <Tooltip title="刷新">
            <SyncOutlined />
          </Tooltip>
        </header>
        <Divider plain style={{ margin: "6px 0" }}></Divider>
        <DirectoryTree
          defaultExpandAll
          onSelect={this.onSelect}
          onExpand={this.onExpand}
          treeData={this.props.treeData}
        />
      </div>
    );
  }
}

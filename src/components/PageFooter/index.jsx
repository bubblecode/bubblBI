import React from "react";

const PageFooter = () => {
  return (
    <div
      style={{
        fontSize: '12px',
        display: 'flex',
        justifyContent: "center",
        alignItems: "center",
        height: "6vh",
      }}
    >
      Copyright © 2021 bubblecode
    </div>
  );
};

export default PageFooter;

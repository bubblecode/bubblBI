import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "dva";
import Toolbar from "../Toolbar";
import "react-grid-layout/css/styles.css";
import "react-resizable/css/styles.css";
import Panel from "../Panel";
import { OPERATINGS } from "../../api/api";

class ProjectPanel extends Component {
  static propTypes = {
    curProject: PropTypes.object,
  };

  state = {
    operating: OPERATINGS.NONE,
  };

  componentDidMount() {
    this.props.dispatch({
      type: "unit/getAllUnits",
      payload: {
        project_id: this.props.curProject.project_id,
      },
    });
  }

  onAddUnit = (projectId) => {
    console.log("添加组件", projectId);
    this.setState((prevState) => ({
      operating:
        prevState.operating === OPERATINGS.CREATE
          ? OPERATINGS.NONE
          : OPERATINGS.CREATE,
    }));
  };

  onEditUnit = (projectId) => {
    console.log("修改组件", projectId);
    this.setState((prevState) => ({
      operating:
        prevState.operating === OPERATINGS.EDIT
          ? OPERATINGS.NONE
          : OPERATINGS.EDIT,
    }));
  };

  onRemoveUnit = (projectId) => {
    console.log("删除组件", projectId);
    this.setState((prevState) => ({
      operating:
        prevState.operating === OPERATINGS.DELETE
          ? OPERATINGS.NONE
          : OPERATINGS.DELETE,
    }));
  };

  render() {
    const { curProject, setting, unit } = this.props;
    return (
      <Fragment>
        <Panel unitList={unit.unit_list} operating={this.state.operating}/>
        <Toolbar
          projectId={curProject.project_id}
          hideDelay={setting.ToolbarAutoHideDelay}
          onAddUnit={this.onAddUnit}
          onEditUnit={this.onEditUnit}
          onRemoveUnit={this.onRemoveUnit}
        />
      </Fragment>
    );
  }
}

export default connect(({ setting, unit }) => ({ setting, unit }))(
  ProjectPanel
);

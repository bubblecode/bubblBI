import React, { useState } from "react";
import PropTypes from "prop-types";
import styles from "./index.scss";
import { EditFilled, DeleteFilled, ToolFilled } from "@ant-design/icons";
import { Tooltip } from "antd";

const Toolbar = ({
  hideDelay,
  projectId,
  onAddUnit,
  onEditUnit,
  onRemoveUnit,
}) => {
  const [isActive, setActive] = useState(false);
  const [activeId, setActiveId] = useState(-1);
  const Item = (props) => (
    <Tooltip title={props.title}>
      <div className={styles.toolbarItem} onClick={props.onClick}>
        {props.children}
      </div>
    </Tooltip>
  );

  const onFocusToolBar = () => {
    setActive(true);
  };

  const onBlurToolBar = () => {
    if (activeId > 0) {
      clearTimeout(activeId);
    }
    const id = setTimeout(() => {
      setActive(false);
    }, hideDelay);
    setActiveId(id);
  };

  return (
    <div
      className={[
        styles.toolbar__base,
        isActive ? styles.toolbar__active : null,
      ].join(" ")}
      onMouseEnter={onFocusToolBar}
      onMouseLeave={onBlurToolBar}
    >
      <Item title="添加组件" onClick={onAddUnit.bind(this, projectId)}>
        <EditFilled />
      </Item>
      <Item title="编辑组件" onClick={onEditUnit.bind(this, projectId)}>
        <ToolFilled />
      </Item>
      <Item title="移除组件" onClick={onRemoveUnit.bind(this, projectId)}>
        <DeleteFilled />
      </Item>
    </div>
  );
};

Toolbar.propTypes = {
  projectId: PropTypes.number,
  onAddUnit: PropTypes.func,
  onEditUnit: PropTypes.func,
  onRemoveUnit: PropTypes.func,
};

export default Toolbar;

import React, { useState, useRef, useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "dva";
import "react-grid-layout/css/styles.css";
import "react-resizable/css/styles.css";
import { Responsive, WidthProvider } from "react-grid-layout";
import Unit from "../PanelUnit/index";
import { OPERATINGS } from "../../api/api";
import styles from "./index.scss";

const ReactResponsiveGridLayout = WidthProvider(Responsive);

function Panel(props) {
  const [curBreakpoint, setCurBreakpoint] = useState("lg");
  const [createStep, setCreateStep] = useState("finish");
  const [ghostInfo, setGhostInfo] = useState({ x: 0, y: 0, w: 1, h: 1 });
  const ContentRef = useRef(null);

  const onBreakpointChange = (breakpoint) => {
    console.log("b", breakpoint, curBreakpoint);
    setCurBreakpoint(breakpoint);
  };

  const onLayoutChange = (layout, layouts) => {
    console.log("l", layout);
  };

  const moveGhostUnit = (x, y) => {
    // 根据当前鼠标位置移动到最近的点
    const alpha = 0.2;
    const width = ContentRef.current.clientWidth;
    const height = ContentRef.current.clientHeight;
    console.log(x, y, x / parseInt(width / 12, 10));
    setGhostInfo({
      ...ghostInfo,
      x: Math.round((x / parseInt(width  / 12, 10)) - alpha),
      y: Math.round((y / parseInt(height / 12, 10))),
    });
  };

  const scaleGhostUnit = (x, y) => {
    // 根据鼠标移动的位置缩放ghostUnit
    console.log(`(${x},${y})`);
  };

  const onDrawUnitBegin = (event) => {
    event.stopPropagation();
    if (props.operating !== OPERATINGS.CREATE) {
      return;
    }
    setCreateStep("begin");
    moveGhostUnit(event.nativeEvent.offsetX, event.nativeEvent.offsetY);
  };

  const onDrawingUnit = (event) => {
    if (
      props.operating !== OPERATINGS.CREATE ||
      event.nativeEvent.buttons !== 1
    ) {
      return;
    }
    setCreateStep("drawing");
    scaleGhostUnit(event.nativeEvent.offsetX, event.nativeEvent.offsetY);
  };

  const onDrawUnitFinish = (event) => {
    if (props.operating !== OPERATINGS.CREATE) {
      return;
    }
    setCreateStep("finish");
  };

  useEffect(() => {
    setTimeout(() => {
      ContentRef.current = document.getElementById("content");
    }, 0);
  }, []);

  return (
    <>
      <div
        id="content"
        className={[
          styles.panel__window,
          props.operating === OPERATINGS.CREATE
            ? styles.panel__window__create
            : "",
        ].join(" ")}
        onMouseDown={onDrawUnitBegin}
        onMouseMove={onDrawingUnit}
        onMouseUp={onDrawUnitFinish}
      >
        <ReactResponsiveGridLayout
          className="layout"
          style={{ border: "1px dashed #00F" }}
          width="100%"
          breakpoints={{ lg: 1200, md: 996, sm: 768, xs: 480, xxs: 0 }}
          cols={{ lg: 12, md: 10, sm: 6, xs: 4, xxs: 2 }}
          items={50}
          rowHeight={50}
          onBreakpointChange={onBreakpointChange}
          onLayoutChange={onLayoutChange}
          measureBeforeMount={true}
          compactType={null}
          preventCollision={false}
        >
          {props.unitList &&
            props.unitList.map((item) => {
              const { unit_info: unitInfo, chart_info: chartInfo } = item;
              return (
                <div
                  key={unitInfo.unit_id}
                  className={styles.panel__unit}
                  data-grid={unitInfo.data_grid}
                >
                  <Unit unitId={unitInfo.unit_id} chartInfo={chartInfo}>
                    unit
                  </Unit>
                </div>
              );
            })}
          <div
            key={`ghost__unit__${parseInt(Math.random() * 100, 10)}`}
            style={{
              visibility:
                props.operating === OPERATINGS.CREATE && createStep !== "finish"
                  ? "visible"
                  : "hidden",
              height: "100px",
              width: "100px",
              backgroundColor: "rgba(255, 0, 0, 0.2)",
            }}
            data-grid={ghostInfo}
          ></div>
        </ReactResponsiveGridLayout>
      </div>
    </>
  );
}

Panel.propTypes = {
  unitList: PropTypes.array,
  operating: PropTypes.number,
};

export default connect(({ setting }) => ({ setting }))(Panel);

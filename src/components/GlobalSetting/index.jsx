import React, { Component } from "react";
import { connect } from "dva";
import { Typography, Divider, Form, InputNumber, Button, Empty } from "antd";

const { Title, Paragraph } = Typography;
const FormItem = Form.Item;
class GlobalSetting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ToolbarAutoHideDelay: this.props.setting.ToolbarAutoHideDelay,
    };
  }

  onAutoHideDelayChange = (value) => {
    this.setState({
      ToolbarAutoHideDelay: value,
    });
  };

  onAutoHideDelayBlur = () => {
    if (!this.state.ToolbarAutoHideDelay) {
      this.setState({
        ToolbarAutoHideDelay: this.props.setting.ToolbarAutoHideDelay,
      });
    }
  };

  onSubmit = async () => {
    const {
      basic: { user_info },
      dispatch,
    } = this.props;
    const { ToolbarAutoHideDelay } = this.state;
    const result = await dispatch({
      type: "setting/updateGlobalConfig",
      payload: {
        user_id: user_info.user_id,
        ToolbarAutoHideDelay,
      },
    });
  };

  render() {
    return (
      <div style={{ padding: "15px", overflow: "auto", height: "100%" }}>
        <Typography>
          <Title level={2} id="header">全局配置</Title>
          <Divider />
          <Title level={3} id="basic-config">基本设置</Title>
          <Paragraph>
            <Empty
              description="暂无配置项"
              image={Empty.PRESENTED_IMAGE_SIMPLE}
            />
          </Paragraph>
          <Title level={3} id="project-config">项目配置</Title>
          <Paragraph>
            <FormItem>
              失去焦点
              <InputNumber
                min={1}
                max={30000}
                size="small"
                value={this.state.ToolbarAutoHideDelay}
                onChange={this.onAutoHideDelayChange}
                onBlur={this.onAutoHideDelayBlur}
              />
              毫秒后，收起底部工具栏。
            </FormItem>
          </Paragraph>
          <Title level={3} id="plugin-config">插件管理</Title>
          <Paragraph>
            <Empty
              description="暂无配置项"
              image={Empty.PRESENTED_IMAGE_SIMPLE}
            />
          </Paragraph>
          <Title level={3} id="advanced-config">高级设置</Title>
          <Paragraph>
            <Empty
              description="暂无配置项"
              image={Empty.PRESENTED_IMAGE_SIMPLE}
            />
          </Paragraph>
          <div style={{ textAlign: "center", width: "100%", padding: "0 36%" }}>
            <Button block type="primary" size="large" onClick={this.onSubmit}>
              保存
            </Button>
          </div>
        </Typography>
      </div>
    );
  }
}

export default connect(({ setting, basic }) => ({ setting, basic }))(
  GlobalSetting
);

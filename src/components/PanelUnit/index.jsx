import React, { useEffect } from "react";
import PropTypes from "prop-types";
import styles from "./index.scss";
import ReactEcharts from "echarts-for-react";
// 引入柱状图
import "echarts/lib/chart/bar";
// 引入提示框和标题组件
import "echarts/lib/component/tooltip";
import "echarts/lib/component/title";

function Unit(props) {
  useEffect(() => {}, []);

  return (
    <div id={`unit__${props.unitId}`} className={styles.unit__base}>
      <ReactEcharts
        option={props.chartInfo}
        style={{
          padding: '5px',
          width: "100%",
          height: "100%",
          backgroundColor: "#FFF",
          borderRadius: "8px",
        }}
      />
    </div>
  );
}

Unit.propTypes = {
  unitId: PropTypes.string,
  chartInfo: PropTypes.object,
};

export default Unit;

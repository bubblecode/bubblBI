import { createFromIconfontCN } from '@ant-design/icons';
import React from 'react';

const IconFontURL = createFromIconfontCN({
  scriptUrl: '//at.alicdn.com/t/font_8d5l8fzk5b87iudi.js',
});

const IconFont = ({ type }) => {
  return (
    <IconFontURL type={type}/>
  );
};

export default IconFont;
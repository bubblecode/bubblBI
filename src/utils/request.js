import axios from "axios";

const prefix = "http://127.0.0.1:5000/server/api";

function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }

  const error = new Error(response.statusText);
  error.response = response;
  throw error;
}

/**
 * @param {string} url
 * @param {string} method
 * @param {object} params
 * @returns object
 */
export default function request(url, method, params) {
  const baseUrl = `${prefix}/${url.indexOf("/") === 0 ? url.substr(1) : url}`;
  switch (method.toUpperCase()) {
    case "GET":
      let extra = "";
      if (params && Object.keys(params).length > 0) {
        extra = Object.keys(params)
          .map((key) => {
            return `${key}=${params[key]}`;
          })
          .join("&");
      }
      return axios
        .get(`${baseUrl}${extra ? `?${extra}` : ""}`)
        .then(checkStatus)
        .then((data) => {
          return data.data;
        })
        .catch((err) => ({ err }));
    case "POST":
      return axios
        .post(baseUrl, params)
        .then(checkStatus)
        .then((data) => {
          return data.data;
        })
        .catch((err) => ({ err }));
    default:
      return axios({
        url: baseUrl,
        method: method,
        data: params,
      })
        .then(checkStatus)
        .then((data) => {
          return data.data;
        })
        .catch((err) => ({ err }));
  }
}

import React from 'react';
import { Router, Route, Switch } from 'dva/router';
import Playground from './routes/Playground';
import ControlPanel from './routes/ControlPanel';

function RouterConfig({ history }) {
  return (
    <Router history={history}>
      <Switch>
        <Route path="/" exact component={Playground} />
        <Route path="/console" exact component={ControlPanel} />
      </Switch>
    </Router>
  );
}

export default RouterConfig;

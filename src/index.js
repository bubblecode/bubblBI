import dva from "dva";
import "./index.css";
import Basic from "./models/basic";
import Project from "./models/project";
import Setting from "./models/setting";
import Unit from "./models/Unit";

// 1. Initialize
const app = dva({});
// 2. Plugins
app.use({});

// 3. Model
app.model(Basic);
app.model(Project);
app.model(Setting);
app.model(Unit);

// 4. Router
app.router(require("./router").default);

// 5. Start
app.start("#root");

const BAR_TYPE = {
  PROJECT: "project",
  PLUGIN: "plugin",
  DESIGN: "design",
  MESSAGE: "message",
  SETTING: "setting",
};

const OPERATINGS = {
  NONE: 0,
  CREATE: 1,
  EDIT: 2,
  DELETE: 3,
};

export { BAR_TYPE, OPERATINGS };
import request from '../utils/request';

export function getProjectTree(params) {
  return request('/project/project_tree', 'get', params);
}


export function getProjectInfo(params) {
  return request('/project/get_project_info', 'get', params);
}

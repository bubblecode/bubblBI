import request from '../utils/request';

export function getAllUnits(params) {
  return request('/unit/get_project_units', 'get', params);
}

export function updateUnit(params) {
  return request('/unit/update_unit', 'post', params);
}
import request from '../utils/request';

export function getConfig(params) {
  return request('/setting/get_config', 'get', params);
}

export function updateConfig(params) {
  return request('/setting/update_config', 'post', params);
}
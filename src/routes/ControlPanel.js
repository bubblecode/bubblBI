import React, { useState, useEffect } from "react";
import { connect } from "dva";
import SideBar from "../components/SideBar/index";
import Contain from "../components/Contain/index";
import { BAR_TYPE } from "../api/api";
function ControlPanel({ dispatch, basic, project }) {
  const [containType, setContainType] = useState(BAR_TYPE.PROJECT);
  const [curProject, setCurProject] = useState({});

  const onContainTypeChange = (type) => {
    if (type !== BAR_TYPE.MESSAGE && type !== containType) {
      setContainType(type);
    }
  };

  const getProjectDetail = projectId => {
    dispatch({
      type: 'project/getProjectInfo',
      payload: {
        project_id: projectId,
      },
    });
  };

  useEffect(() => {
    dispatch({ 
      type: 'basic/getUserData',
      payload: {} 
    });
    
  }, []);

  useEffect(() => {
    dispatch({
      type: "setting/getGlobalConfig",
      payload: {
        user_id: basic.user_info.user_id,
      },
    });
  }, [basic.user_info])

  useEffect(() => {
    setCurProject(project.cur_project);
  }, [project.cur_project]);

  return (
    <div
      style={{
        display: "flex",
        height: "100%",
      }}
    >
      <SideBar
        curType={containType}
        onContainTypeChange={onContainTypeChange}
        onProjectClick={getProjectDetail}
      />
      <Contain curType={containType} curProject={curProject} />
    </div>
  );
}

ControlPanel.propTypes = {};

export default connect(({ basic, project }) => ({ basic, project }))(ControlPanel);

import React from "react";
import { connect } from "dva";
import { Link } from "react-router-dom";

function Playground() {
  return (
    <div>
      <p>主页</p>
      <Link to="/console">转到</Link>
    </div>
  );
}

Playground.propTypes = {
};

export default connect()(Playground);
